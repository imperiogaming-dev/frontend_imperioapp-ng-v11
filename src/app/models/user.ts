export class User{
    constructor(
        public _id: string,
        public userName: string,
        public pass: string,
        public email: string
    ){}
}