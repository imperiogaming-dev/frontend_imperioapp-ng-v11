import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    { provide: Window, useValue: window }
]
})
export class AppComponent implements OnInit {
  title = 'imperioApp';
  constructor(
    public ROUTER: Router
  ){

  }
  ngOnInit(){
  }
}
