import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService} from '../../services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public login: any;
  constructor(
    private ROUTER: Router,
    private ADMINSERVICE: AdminService,
  ) {
    this.login = {
      email: '',
      pass: ''
    };
  }

  ngOnInit(): void {
  }
  onSubmit(){
    this.ADMINSERVICE.login(this.login).subscribe(
      response => {
        if ( response.status === 'Ok'){
          debugger;
          //const userInDB = response.data[0];
          alert('Bienvenido: ' + response.data.user_name);
          this.ROUTER.navigate(['/home']);
        }else{
          alert('Nonas: ');
        }
      },
      error => {
        console.log(error);
        alert('usuario o contraseña incorrectos');
      }
    );
  }
}
